/*
 * OSLauncher.c
 *
 *  Created on: Apr 2, 2016
 *      Author: Venkat
 */
#include <stdint.h>
#include "ProjectControl.h"
#include "OS_Kernel/PIDControlManager/PID.h"

#ifdef MAIN_OS

#include <stdint.h>
#include "OS_Kernel/OS.h"
#include "Shell/Shell.h"
#include "OS_Kernel/BackgroundThreadManager/OS_BackgroundThread.h"


int main()
{
	OS_Init();
	OS_AddThread(IdleTask, 100, 5);
	OS_AddThread(CAN_Thread, 100, 2);
	OS_AddThread(SonarController, 100, 1);
	OS_AddThread(IRController, 100, 1);
	OS_AddPeriodicThread(SteeringController, TIME_1MS * 200, 1);
	OS_Launch(TIME_1MS);

}
#endif
