/*
 * Shell.c
 *
 *  Created on: Feb 1, 2016
 *      Author: Venkat
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "Shell.h"
#include "../OS_FileSys/OS_FileSystemScheme.h"
#include "../OS_FileSys/OS_FileSystemScheme.h"
#include "../OS_Kernel/HardwareManager/OS_Hardware.h"
#include "../OS_Kernel/OS.h"
#include "../OS_Kernel/ProcessLoaderManager/loader.h"

#define COMMAND_COUNT 4

typedef void (*fp)(int); //Declares a type of a void function that accepts an int
extern void OutCRLF(void);

// Command Functions
void Help_Output(int one);
void Shell_LoadProcess(int one);
void Shell_SetServo(int one);
void Shell_SetLMotorSpeed(int one);
void Shell_SetRMotorSpeed(int one);

char*			commandDef[]			=		{
												"help: 				Output command information.",
												"setservo:			Sets the servo angle.",
												"setRspeed:         Sets the right motor.",
												"setLspeed:         Sets the left motor."

											};
char* 			commandFormat[]		= 		{
												"help",
												"setservo",
												"setRspeed",
												"setLspeed"
											};
char* 			commands[] 			= 		{
												"help",
												"setservo",
												"motorleft",
												"motorright"
											};

fp 				function_array[] 	= 		{
												Help_Output,
												Shell_SetServo,
												Shell_SetRMotorSpeed,
												Shell_SetLMotorSpeed
											};

unsigned int		CommandCount[]	    =       {
												0,
												1,
												1,
												1
											};

char CommandTokens[PARAMS_MAX_NUM][PARAMS_MAX_SIZE];
unsigned int CurrentCommandParamCount;



char Command[COMMAND_MAX_SIZE];
char ExecuteName[PARAMS_MAX_SIZE];


void OutCRLF(void)
{
	OS_SerialOutChar(CR);
	OS_SerialOutChar(LF);
}

void Interpreter()
{
	  OS_SerialOutString("Please enter command:");
	  //ADC_Open(0, 1000);
	  OutCRLF();
	  while (1)
	  {
		  OS_SerialIn(Command, COMMAND_MAX_SIZE);

		  //uint32_t PreviousState = StartCritical();
		  Shell_CommandTokenize(Command);

		  Shell_StringRegex(CommandTokens[0], ExecuteName);
		  int match = Shell_CommandNumber(ExecuteName);
		  if (match >= 0)
		  {
			  Shell_RunCommand(match);
		  }
		  else if (match == -1)
		  {
			  OutCRLF();
			  OS_SerialOutString("Command not found.");
		  }
		  else
		  {
				OutCRLF();
				OS_SerialOutString("ERROR: Command parameters not correct.");
		  }

		  OutCRLF();
		  OutCRLF();
		  //EndCritical(PreviousState);

		  OS_SerialOutString("Please enter command:");
	  }

}

void Shell_CommandTokenize(char* pCommand)
{
	memset(CommandTokens, 0, sizeof(CommandTokens[0][0]) * PARAMS_MAX_NUM * PARAMS_MAX_SIZE);

	unsigned int TokenIterator = 0;

	char* Token = strtok(pCommand, " ");

	while (Token)
	{

		strcpy(CommandTokens[TokenIterator], Token);

		Token = strtok(NULL, " ");

	    TokenIterator++;
	}

	CurrentCommandParamCount = TokenIterator - 1; // Do not count the command name itself

}

void Shell_FreeTokens(char** tokens)
{
	char** token_back = tokens;

	while(*tokens)
	{
		free(*tokens);
		tokens++;
	}

	free(token_back);

}

int Shell_CommandNumber(char* commandString)
{
	int fnIterator = 0;
	for (fnIterator = 0; fnIterator < COMMAND_COUNT; fnIterator++)
	{
		unsigned int compare = strcmp(commandString, commands[fnIterator]);
		if (compare == 0)
		{
			if (CurrentCommandParamCount != CommandCount[fnIterator])
			{
				return -2;
			}
			return fnIterator;
		}
	}

	return -1;
}

void Shell_StringRegex(char* src, char* dst) // Null terminated strings
{
	for (; *src; src++) {
	   if ('a' <= *src && *src <= 'z'
	    || '0' <= *src && *src <= '9'
	    || *src == '_') *dst++ = *src;
	}
	*dst = '\0';
}

void Shell_RunCommand(unsigned int Index)
{
	function_array[Index](0);
}

void Help_Output(int one)
{
	int i = 0;
	OutCRLF();
	OutCRLF();
	OS_SerialOutString("Command Information:");
	OutCRLF();
	for (i = 0; i < COMMAND_COUNT; i++)
	{
		OS_SerialOutString(commandDef[i]);
	}

	OS_SerialOutString("Command Formats:");
	OutCRLF();
	for (i = 0; i < COMMAND_COUNT; i++)
	{
		OS_SerialOutString(commandFormat[i]);
	}
}

void Shell_SetServo(int one)
{
	OS_SetSteeringAngle(atoi(CommandTokens[1]));
}

void Shell_SetLMotorSpeed(int one)
{
	OS_SetLeftMotor(atoi(CommandTokens[1]));
}

void Shell_SetRMotorSpeed(int one)
{
	OS_SetRightMotor(atoi(CommandTokens[1]));
}
