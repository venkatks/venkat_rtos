/*
 * OS_Semaphore.h
 *
 *  Created on: Mar 3, 2016
 *      Author: Venkat
 */

#ifndef NRTOS5_OS_KERNEL_MUTUALEXCLUSIONMANAGER_OS_SEMAPHORE_H_
#define NRTOS5_OS_KERNEL_MUTUALEXCLUSIONMANAGER_OS_SEMAPHORE_H_

#ifndef LAB3_VRTOS_OS_CRITICAL_OS_LINKEDLIST_OS_LINKEDLIST_H_
#include "../../OS_Kernel/PriorityManager/OS_LinkedList/OS_LinkedList.h"
#endif

#ifndef OS_KERNEL_OS_H_
#include "../OS.h"
#endif


typedef struct nRTOS_Mutex
{
	uint32_t permits;

	ThreadQueue BlockedThreads;

} Semaphore;

// Semaphore functions
Semaphore* 	OS_InitSemaphore(uint32_t numPermits);
void 		OS_Wait(Semaphore* SemToWaitOn);	// Utilizes one permit per thread;
void 		OS_Signal(Semaphore* SemToWaitOn);
void 		OS_WaitMultiple(Semaphore* sem1, Semaphore* sem2);


// Intra-Kernel Functions
void 		_OS_AddThreadToSemaphore(Semaphore* Sem4, TCB* ThreadToAdd);
void 		_OS_RemoveThreadFromSemaphore(Semaphore* Sem4, TCB* ThreadToRemove);



#endif /* NRTOS5_OS_KERNEL_MUTUALEXCLUSIONMANAGER_OS_SEMAPHORE_H_ */
