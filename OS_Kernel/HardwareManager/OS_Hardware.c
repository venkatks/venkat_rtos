/*
 * OS_Hardware.c
 *
 *  Created on: Mar 3, 2016
 *      Author: Venkat
 */
#include <stdio.h>
#include <stdint.h>
#include "../../OS_Kernel/HardwareManager/ADCDriver/ADC.h"
#include "../../OS_Kernel/HardwareManager/LCDDriver/ST7735.h"
#include "../../OS_Kernel/HardwareManager/LCDDriver/ST7735x.h"
#include "../../OS_Kernel/HardwareManager/LEDDriver/LED.h"
#include "../../OS_Kernel/HardwareManager/SystemOscillatorDriver/PLL.h"
#include "../../OS_Kernel/HardwareManager/UARTDriver/UART.h"
#include "../../OS_Kernel/MutualExclusionManager/OS_Semaphore.h"
#include "CANDriver/can0.h"
#include "../../OS_Kernel/OS.h"
#include "OS_Hardware.h"
#include "MotorDriver/MainMotor.h"
#include "ServoDriver/Servo.h"
#include "MotorDriver/MainMotor.h"
#include "../PIDControlManager/PID.h"
#include "../MemoryManager/OS_Allocation.h"

Semaphore* LCD_Sem4;
Semaphore* CAN_Sem4;

void OS_HardwareInit(void)
{
	PLL_Init(Bus80MHz);
	UART_Init();
	Output_Init();
	LED_Init();
	CAN0_Open();
	Motor_Init(24000);
	OS_SetMotorMovement(1000, BACKWARD);
	Servo_Init();
	Servo_SetAngle(1800); // Set the servo to the starting position
	CAN0_Open();
	LCD_Sem4 = OS_InitSemaphore(1);
	CAN_Sem4 = OS_InitSemaphore(1);

	if (LCD_Sem4 == 0 || CAN_Sem4 == 0) OS_KernelPanic(HEAP_OUTOFMEMORY, "Semaphore init");
}



// Shell functions
void OS_SerialOutString(char* string)
{
	UART_OutString(string);
}

void OS_SerialOutChar(char data)
{
	UART_OutChar(data);
}

void OS_SerialOutUDec(uint32_t dec)
{
	UART_OutUDec(dec);
}

void OS_SerialIn(char* bufferArray, uint16_t bufferSize)
{
	UART_InString(bufferArray, bufferSize);
}

void OS_SerialOutMessage(unsigned int device, unsigned int line, char *string, unsigned long value)
{
	OS_Wait(LCD_Sem4);
	OS_SerialOutString("\n::::::USER TASK: ");
	OS_SerialOutString(string);
	OS_SerialOutString(" ");
	OS_SerialOutUDec(value);
	OS_SerialOutString(" ");
	OS_SerialOutString("::::::\n");
	OS_Signal(LCD_Sem4);

}

// LCD Functions
void OS_DisplayOutString(char* string)
{
	OS_Wait(LCD_Sem4);
	ST7735_OutString(string);
	OS_Signal(LCD_Sem4);
}

void OS_DisplayOutChar(char c)
{
	OS_Wait(LCD_Sem4);
	ST7735_OutChar(c);
	OS_Signal(LCD_Sem4);
}

void OS_DisplayOutUDec(uint32_t dec)
{
	OS_Wait(LCD_Sem4);
	ST7735_OutUDec(dec);
	OS_Signal(LCD_Sem4);
}

void OS_DisplayMessage(unsigned int device, unsigned int line, char *string, unsigned long value)
{
	OS_Wait(LCD_Sem4);
	ST7735_Message(device, line, string, value);
	OS_Signal(LCD_Sem4);
}

// ADC Functions
uint8_t 	ADC_Open(uint8_t ChannelNumber)
{
	return _ADC_Open(ChannelNumber);
}

void ADC_Collect(uint32_t ChannelNum, uint32_t Period, void(*ADCTask)(uint32_t data))
{
	_ADC_Collect(ChannelNum, Period, ADCTask);
}

uint32_t ADC_In()
{
	return _ADC_In();
}

// CAN Functions
extern void OutCRLF(void);
uint16_t RcvData[CAN_MSG_SIZE];
//char buffer[40];
void CAN_Thread(void)
{	//CANmain
  while(1) {
    if(CAN0_GetMailNonBlock(RcvData))
    {
			//PF3 ^= 0x08;
			// RcvData is now available
			uint16_t pingsensor = RcvData[0] & 0x8000;
			RcvData[0] &= 0x7FFF;
			if(pingsensor)
			{
				PING_DATA* NewSonarData = OS_NewAllocation(sizeof(PING_DATA));

				if (NewSonarData == 0)
				{
					continue;
				}

				NewSonarData->PingSensorZero 		= RcvData[0];
				NewSonarData->PingSensorOne	 		= RcvData[1];

				PID_NewData(SONAR_SENSOR, NewSonarData);

			}
			else
			{
				IR_DATA* NewIRData = OS_NewAllocation(sizeof(IR_DATA));

				if (NewIRData == 0)
				{
					continue;
				}

				NewIRData->IRSensorZero 		= RcvData[0];
				NewIRData->IRSensorOne 		= RcvData[1];
				NewIRData->IRSensorTwo 		= RcvData[2];
				NewIRData->IRSensorThree 	= RcvData[3];

				PID_NewData(IR_SENSOR, NewIRData);
			}
    }
  }
}



// Servo Drivers
void OS_SetSteeringAngle(int angle)
{
	// TODO: Do the conversion here
	if (angle < 610) angle = 610;
	if (angle > 3000) angle = 3000;

	Servo_SetAngle(angle);
}

// Motor Drivers

void OS_SetLeftMotor(int32_t speed)
{
	FirstMotor_SetMovement(speed, MTR1_FORWARD);
}

void OS_SetRightMotor(int speed)
{
	SecondMotor_SetMovement(speed, MTR2_FORWARD);
}

void OS_SetRightMotorDir(int speed, enum MotorDirection direction)
{
	if (speed > 24000) speed = 24000;
	SecondMotor_SetMovement(speed, (direction == FORWARD) ? MTR2_FORWARD : MTR2_BACKWARD);
}

void OS_SetLeftMotorDir(int speed, enum MotorDirection direction)
{
	if (speed > 24000) speed = 24000;
	FirstMotor_SetMovement(speed, (direction == FORWARD) ? MTR1_FORWARD : MTR1_BACKWARD);
}

void OS_SetMotorMovement(uint64_t Speed, enum MotorDirection direction)
{
	Speed = Speed % (PWM_PERIOD - 10);
	if (Speed < 2) Speed = 0 + 10;

	if (direction == FORWARD)
	{
		FirstMotor_SetMovement(Speed, MTR1_FORWARD);
		SecondMotor_SetMovement(Speed, MTR2_FORWARD);
	}
	else
	{
		FirstMotor_SetMovement(Speed, MTR1_BACKWARD);
		SecondMotor_SetMovement(Speed, MTR2_BACKWARD);
	}
}
