/* This example accompanies the book
   "Embedded Systems: Real Time Interfacing to ARM Cortex M Microcontrollers",
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2013
  Program 6.7, section 6.3.2

 Copyright 2013 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */
#include <stdint.h>
#include "MainMotor.h"
#include "../../../inc/tm4c123gh6pm.h"

#define GPIO_PB7      (*((volatile unsigned long *)0x40005200))
#define GPIO_PB5      (*((volatile unsigned long *)0x40005080))

void FirstMotor_SetMovement(uint64_t duty, enum __Motor1Direction direction);
void SecondMotor_SetMovement(uint64_t duty, enum __Motor2Direction direction);


void Motor_Init(unsigned short duty)
{   volatile unsigned long delay;

	SYSCTL_RCGCPWM_R |= SYSCTL_RCGCPWM_R0;
    SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R1;
    delay = SYSCTL_RCGCGPIO_R;
	GPIO_PORTB_AFSEL_R |= 0x50;
	GPIO_PORTB_AFSEL_R &= ~0xA0;
	GPIO_PORTB_DIR_R	|= 0xA0;
	GPIO_PORTB_PCTL_R = (GPIO_PORTB_PCTL_R &~0x0F0F0000) | 0x04040000;
	GPIO_PORTB_AMSEL_R &= ~0xF0;
	GPIO_PORTB_DEN_R |= 0xF0;
	GPIO_PB5 = 0;
	GPIO_PB7 = 0;
	SYSCTL_RCC_R |= SYSCTL_RCC_USEPWMDIV;
	SYSCTL_RCC_R &= ~SYSCTL_RCC_PWMDIV_M;
	SYSCTL_RCC_R |= SYSCTL_RCC_PWMDIV_2;
	PWM0_0_CTL_R = PWM0_1_CTL_R = 0;
	PWM0_0_GENA_R = (PWM_0_GENA_ACTCMPAD_ONE|PWM_0_GENA_ACTLOAD_ZERO);
	PWM0_1_GENA_R = (PWM_1_GENA_ACTCMPAD_ONE|PWM_1_GENA_ACTLOAD_ZERO);
	PWM0_0_LOAD_R = PWM0_1_LOAD_R = PWM_PERIOD - 1;
	PWM0_0_CMPA_R = PWM0_1_CMPA_R = duty - 1;
	PWM0_0_CTL_R |= PWM_0_CTL_ENABLE;
	PWM0_1_CTL_R |= PWM_1_CTL_ENABLE;
	PWM0_ENABLE_R |= (PWM_ENABLE_PWM0EN | PWM_ENABLE_PWM2EN);
}



void FirstMotor_SetMovement(uint64_t duty, enum __Motor1Direction direction)
{
	long value ;
	GPIO_PB7 = (direction != 0)<<7;
	value = (direction)?(PWM_PERIOD - duty):(duty);
	PWM0_0_CMPA_R = value;
}


void SecondMotor_SetMovement(uint64_t duty, enum __Motor2Direction direction)
{

	long value;
	GPIO_PB5 = (direction != 0)<<5;
	value = (direction)?(PWM_PERIOD - duty):(duty);
	PWM0_1_CMPA_R = value;
}

void BothMotors_Stop(void)
{
	GPIO_PB7 = 0;
	GPIO_PB5 = 0;
	PWM0_0_CMPA_R = 2;
	PWM0_1_CMPA_R = 2;
}

