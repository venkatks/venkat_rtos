// motor.h
// Runs on TM4C123
// Use PWM0/PB6 to generate pulse-width modulated outputs.
// Daniel Valvano
// September 3, 2013

/* This example accompanies the book
   "Embedded Systems: Real Time Interfacing to ARM Cortex M Microcontrollers",
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2013
  Program 6.7, section 6.3.2

 Copyright 2013 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */

#ifndef __MAINMOTOR_H__
#define __MAINMOTOR_H__

#define MOTOR_CCW 1
#define MOTOR_DIFF   750
#define PWM_PERIOD 25000

enum __Motor1Direction
{
	MTR1_FORWARD = 0,
	MTR1_BACKWARD = 1
};

enum __Motor2Direction
{
	MTR2_BACKWARD = 0,
	MTR2_FORWARD  = 1
};

void Motor_Init(unsigned short duty);
void FirstMotor_SetMovement(uint64_t duty, enum __Motor1Direction direction);
void SecondMotor_SetMovement(uint64_t duty, enum __Motor2Direction direction);

#endif
