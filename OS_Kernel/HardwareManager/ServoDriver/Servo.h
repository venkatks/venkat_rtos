/*
 * Servo.h
 *
 *  Created on: Apr 18, 2016
 *      Author: Venkat
 */

#ifndef SERVO_H_
#define SERVO_H_


void Servo_Init();
void Servo_SetAngle(uint32_t dutyCycle);

#endif /* SERVO_H_ */
