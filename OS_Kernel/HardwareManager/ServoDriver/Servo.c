#include <stdint.h>
#include "../../../inc/tm4c123gh6pm.h"

void Servo_Init()
{
	uint32_t initDuty = 125 * (0 + 90) / 9 + 624;

	SYSCTL_RCGCPWM_R |= 0x00000001;
	SYSCTL_RCGCGPIO_R |= 0x00000008;
	while((SYSCTL_PRGPIO_R&0x00000008) == 0){};
	GPIO_PORTD_AFSEL_R |= 0x00000003;
	GPIO_PORTD_PCTL_R &= ~0x000000FF;
	GPIO_PORTD_PCTL_R |= 0x00000044;
	GPIO_PORTD_AMSEL_R &= ~0x00000003;
	GPIO_PORTD_DEN_R |= 0x00000003;
	SYSCTL_RCC_R |= SYSCTL_RCC_USEPWMDIV;
	SYSCTL_RCC_R &= ~SYSCTL_RCC_PWMDIV_M;
	SYSCTL_RCC_R |= SYSCTL_RCC_PWMDIV_2;
	SYSCTL_RCC_R |= SYSCTL_RCC_PWMDIV_64;

	PWM0_3_CTL_R &= ~0x00000002;
	PWM0_3_GENA_R |= 0x0000008C;
	PWM0_3_GENB_R |= 0x0000080C;
	PWM0_3_LOAD_R |= 25000;
	PWM0_3_CMPA_R |= PWM0_3_LOAD_R - initDuty;
	PWM0_3_CMPB_R |= 0x0000014A;
	PWM0_3_CTL_R |= 0x00000001;
	PWM0_ENABLE_R |= 0x000000C0;
}

void Servo_SetAngle(uint32_t dutyCycle)
{
	// 610:  512 ms (minimum)
	// 800:  632 ms
	// 1000: 800 ms
	// 1200: 1 ms
	// 1400: 1.12 ms
	// 1600: 1.30 ms
	// 1800: 1.46 ms
	// 2000: 1.63 ms
	// 2200: 1.78 ms
	// 2400: 1.92 ms
	// 2600: 2.10 ms
	// 2800: 2.24 ms
	// 3000: 2.46 ms (maximum)

	PWM0_3_CMPA_R = PWM0_3_LOAD_R - dutyCycle; // ~1% duty cycle
}
