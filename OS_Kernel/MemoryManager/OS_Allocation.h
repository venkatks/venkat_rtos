/*
 * OS_Allocation.h
 *
 *  Created on: Feb 29, 2016
 *      Author: Venkat
 */

#ifndef NRTOS5_OS_KERNEL_MEMORYMANAGER_OS_ALLOCATION_H_
#define NRTOS5_OS_KERNEL_MEMORYMANAGER_OS_ALLOCATION_H_

void  OS_AllocationInit();
void* OS_NewAllocation(uint64_t numBytes);
void OS_DestroyAllocation(void* allocation, uint64_t numBytes);
void OS_DestroyAllocationNonTracked(void* allocation);



#endif /* NRTOS5_OS_KERNEL_MEMORYMANAGER_OS_ALLOCATION_H_ */
