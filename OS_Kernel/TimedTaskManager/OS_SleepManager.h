/*
 * OS_SleepManager.h
 *
 *  Created on: Mar 1, 2016
 *      Author: Venkat
 */

#ifndef NRTOS5_OS_KERNEL_TIMEDTASKMANAGER_OS_SLEEPMANAGER_H_
#define NRTOS5_OS_KERNEL_TIMEDTASKMANAGER_OS_SLEEPMANAGER_H_

#ifndef OS_CRITICAL_OS_H_
#include "../../OS_Kernel/OS.h"
#endif

void OS_SleepManagerInit(void);
void OS_NewSleepingThread(TCB* pTCB_SleepingThread);





// Should not be called by any part of the kernel except for the systemtimer
void _INTERNAL_OS_SleepManager(void);



#endif /* NRTOS5_OS_KERNEL_TIMEDTASKMANAGER_OS_SLEEPMANAGER_H_ */
