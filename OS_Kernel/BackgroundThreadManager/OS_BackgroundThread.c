/*
 * Scheduler.c
 *
 *  Created on: Feb 3, 2016
 *      Author: Venkat
 */





#include "../../OS_Kernel/BackgroundThreadManager/OS_BackgroundThread.h"
#include "../../inc/tm4c123gh6pm.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "../../OS_Kernel/BackgroundThreadManager/PortFDriver/PortF_Edge.h"
#include "../../OS_Kernel/BackgroundThreadManager/PortFDriver/PortF_Edge.h"
#include "../../OS_Kernel/BackgroundThreadManager/Timer2Driver/Timer2.h"
#include "../../OS_Kernel/BackgroundThreadManager/Timer3Driver/Timer3.h"
#include "../../Venkatware/venkatlib.h"

unsigned int ThreadCount = 0;

uint8_t NumberOfPeriodicThreads = 0;

bool PeriodicThread1InUse = FALSE;
bool PeriodicThread2InUse = FALSE;

bool OS_AddPeriodicThread(void(*timerTask)(void), uint32_t interruptPeriodMS, uint32_t pri)
{
	if (NumberOfPeriodicThreads == 2) return FALSE;
	if (!PeriodicThread1InUse)
	{
		Timer2_Init(timerTask, interruptPeriodMS, pri);
		PeriodicThread1InUse = TRUE;
	}
	else if (!PeriodicThread2InUse)
	{
		Timer3_Init(timerTask, interruptPeriodMS, pri);
		PeriodicThread2InUse = TRUE;
	}

	return TRUE;
}

void DisableTask1(void)
{
	  TIMER2_CTL_R = 0x00000000;
}
void DisableTask2(void)
{
	  TIMER3_CTL_R = 0x00000000;
}
void EnableTask1(void)
{
	  if (PeriodicThread1InUse)  TIMER2_CTL_R = 0x00000001;
}
void EnableTask2(void)
{
	  if (PeriodicThread2InUse)  TIMER3_CTL_R = 0x00000001;

}


void OS_AddSW1Task(void(*taskToExecute)(void), uint64_t priority)
{
	uint32_t Start = StartCritical();
	if (PortFEdge_GetInitState() == FALSE) PortFEdge_Init(priority);
	PortFEdge_SetFunction1(taskToExecute);
	EndCritical(Start);
}

void OS_AddSW2Task(void(*taskToExecute)(void), uint64_t priority)
{
	uint32_t Start = StartCritical();
	if (PortFEdge_GetInitState() == FALSE) PortFEdge_Init(priority);
	PortFEdge_SetFunction2(taskToExecute);
	EndCritical(Start);
}

void _OS_InitializePortF()
{
	if (PortFEdge_GetInitState() == FALSE) PortFEdge_Init(DEFAULT_PORTF_PRI);
}

