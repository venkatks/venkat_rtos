/*
 * PID.h
 *
 *  Created on: Apr 19, 2016
 *      Author: Venkat
 */

#ifndef NRTOS5_OS_KERNEL_PIDCONTROLMANAGER_PID_H_
#define NRTOS5_OS_KERNEL_PIDCONTROLMANAGER_PID_H_

#define SensorFIFOSize 		100
#define SensorFIFOSuccess 	1
#define SensorFIFOFail		0


// Type of data
typedef enum sensorinput
{
	SONAR_SENSOR,
	IR_SENSOR
} SENSOR_TYPE;



typedef struct irsensordata
{
	uint16_t IRSensorZero;
	uint16_t IRSensorOne;
	uint16_t IRSensorTwo;
	uint16_t IRSensorThree;
} IR_DATA;

typedef struct pingsensordata
{
	uint16_t PingSensorZero;
	uint16_t	 PingSensorOne;
} PING_DATA;

void PID_Init();

// Controller Thread
void 	IRController(void);
void 	SonarController(void);
void 	SteeringController(void);

// Inform the controller regarding new data
void 	PID_NewData(SENSOR_TYPE SensorType, void* SensorData);

#endif /* NRTOS5_OS_KERNEL_PIDCONTROLMANAGER_PID_H_ */
