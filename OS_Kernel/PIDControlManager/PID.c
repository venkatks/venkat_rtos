/*
 * PID.c
 *
 *  Created on: Apr 19, 2016
 *      Author: Venkat
 */
#include <stdio.h>
#include <limits.h>
#include "../../Venkatware/FIFO.h"
#include "../MutualExclusionManager/OS_Semaphore.h"
#include "PID.h"
#include "../OS.h"
#include "../HardwareManager/OS_Hardware.h"
#include "../MemoryManager/OS_Allocation.h"
#include "../../driverlib/fpu.h"
#include "../../inc/tm4c123gh6pm.h"
//#define MIN(a,b) (((a)<(b))?(a):(b))
//#define MAX(a,b) (((a)>(b))?(a):(b))


Semaphore* IRMutex 			= 0;
Semaphore* IRCount			= 0;

Semaphore* SteeringMutex 	= 0;
Semaphore* SteeringCount		= 0;

Semaphore* SonarMutex		= 0;
Semaphore* SonarCount		= 0;
Semaphore* SonarDependency	= 0;
AddIndexFifo(IRData, SensorFIFOSize, uint32_t, SensorFIFOSuccess,SensorFIFOFail)
AddIndexFifo(SonarData, SensorFIFOSize, uint32_t, SensorFIFOSuccess,SensorFIFOFail)
AddIndexFifo(IRBaseLeft, SensorFIFOSize, uint32_t, SensorFIFOSuccess, SensorFIFOFail)
AddIndexFifo(IRBaseRight, SensorFIFOSize, uint32_t, SensorFIFOSuccess, SensorFIFOFail)

void PID_Init()
{
	// Semaphores that protect the IR data fifo
	IRMutex			= OS_InitSemaphore(1);
	IRCount			= OS_InitSemaphore(0);

	// Semaphores that protect the steering controller fifo
	SteeringMutex	= OS_InitSemaphore(1);
	SteeringCount	= OS_InitSemaphore(0);

	// Semaphores that protect the sonar data fifo
	SonarMutex		= OS_InitSemaphore(1);
	SonarCount 		= OS_InitSemaphore(0);

	SonarDependency = OS_InitSemaphore(0);
	if (IRMutex == 0) OS_KernelPanic(HEAP_OUTOFMEMORY, "IR Mutex not initalized");
	if (IRCount == 0) OS_KernelPanic(HEAP_OUTOFMEMORY, "IR count not initalized");
	if (SteeringMutex == 0) OS_KernelPanic(HEAP_OUTOFMEMORY, "Steering mutex not initalized");
	if (SteeringCount == 0) OS_KernelPanic(HEAP_OUTOFMEMORY, "Steering count not initalized");
	if (SonarMutex == 0) OS_KernelPanic(HEAP_OUTOFMEMORY, "Sonar mutex Mutex not initalized");
	if (SonarCount == 0) OS_KernelPanic(HEAP_OUTOFMEMORY, "Sonar count not initalized");

	IRDataFifo_Init();
	//SonarDataFifo_Init();
	IRBaseLeftFifo_Init();
	IRBaseRightFifo_Init();
	OS_SetMotorMovement(10000, FORWARD);
}


void PID_NewData(SENSOR_TYPE SensorType, void* SensorData)
{
	if (SensorType == IR_SENSOR)
	{
		OS_Wait(IRMutex);
		uint8_t success = IRDataFifo_Put((uint32_t) SensorData);
		if (success == SensorFIFOFail)
		{
			OS_DestroyAllocation(SensorData, sizeof(IR_DATA));
		}
		OS_Signal(IRMutex);
		OS_Signal(IRCount);
	}
	else
	{
		OS_Wait(SonarMutex);
		uint8_t success = SonarDataFifo_Put((uint32_t) SensorData);

		if (success == SensorFIFOFail)
		{
			OS_DestroyAllocation(SensorData, sizeof(PING_DATA));
		}

		OS_Signal(SonarMutex);
		OS_Signal(SonarCount);

	}
}

uint16_t PreviousSonarData = 0;

short IntTerm;     // accumulated error, RPM-sec
short PrevError;   // previous error, RPM
short Coeff[3];    // PID coefficients
short Actuator;

#define DEAD_CENTER 1825

#define FILTERCOUNT 5
char buffer[50];
uint32_t dataRatios[FILTERCOUNT];
uint8_t  numData = 0;

// PID Constants for IR
#define epsilon 	0.01		// 0.01
#define dt		0.005    // 100 ms loop time
#define MAX		2950
#define MIN		650
#define Kp		0.4   //0.9  // 0.1
#define Kd		0.1 //0.1 	// 0.01
#define Ki		0.005		// 0.005
#define set_pt	1.0
#define FACTOR	1000.0

// PID Constants for Sonar
#define sonar_pt 1.0
uint8_t timeDone = 0;
uint8_t done = 0;
void* run = 0;
uint32_t timeCompleted = 0;
void SteeringController(void)
{
	uint32_t time = OS_MsTime();
	UART_OutUDec(time);
	UART_OutString("\n");
	if (OS_MsTime() >= 60000)
	{
		timeCompleted++;
		OS_ClearMsTime();
	}
	if(timeCompleted >= 6)
	{
		OS_SetLeftMotor(500);
		OS_SetRightMotor(500);
		timeDone = 1;
		DisableInterrupts();
		//PWM0_0_CTL_R = 0;
		//PWM0_1_CTL_R = 0;
		while(1){}
	}

	FPUEnable();
	static float prevLeftDistance = -1;
	static float prevRightDistance = -1;
	static int initStarted = 0;
	while(IRBaseLeftFifo_Size() > 0 && IRBaseRightFifo_Size() > 0) /* See if there's new steering data */
	{
		uint32_t leftDistance;
		uint32_t rightDistance;

		if (initStarted)
		{
			uint8_t success = IRBaseLeftFifo_Get(&leftDistance); /* Get the new IR ratios */
					success = IRBaseRightFifo_Get(&rightDistance);

			float resultLeft = prevLeftDistance - leftDistance;
			float resultRight = prevRightDistance - rightDistance;

			if (fabs(resultLeft) < fabs(resultRight))
			{
				if (resultRight < 0)
				{
					OS_SetSteeringAngle(DEAD_CENTER - 600 * fabs(resultRight));
				}
				else if (resultRight == 0)
				{
					OS_SetSteeringAngle(DEAD_CENTER);
				}
				else
				{
					OS_SetSteeringAngle(DEAD_CENTER + 600 * fabs(resultRight));
				}
			}
			else
			{
				if (resultLeft < 0)
				{
					OS_SetSteeringAngle(DEAD_CENTER + 600 * fabs(resultLeft));
				}
				else if (resultLeft == 0)
				{
					OS_SetSteeringAngle(DEAD_CENTER);
				}
				else
				{
					OS_SetSteeringAngle(DEAD_CENTER - 600 * fabs(resultLeft));

				}
			}
		    prevLeftDistance += leftDistance;
		    prevRightDistance += rightDistance;

		}
		else
		{
			OS_SetSteeringAngle(DEAD_CENTER);
			initStarted = 1;

			uint8_t success = IRBaseLeftFifo_Get(&leftDistance); /* Get the new IR ratios */
					success = IRBaseRightFifo_Get(&rightDistance);

		    prevLeftDistance = leftDistance;
		    prevRightDistance = rightDistance;

		}



	}

	FPUDisable();
}

#define STR_SPEED 24
#define TRN_SPEED 24

uint32_t speedMultiplier = 1000;

uint32_t latestIRLeft 	= 0;
uint32_t latestIRRight 	= 0;
uint32_t latestSonar		= 1000;

#define IR_THRESHOLD 15
#define SONAR_THRESHOLD 3
#define OUT_OF_THRESHOLD 7
void IRController(void)
{
	static int32_t turnMultiplier 	= 1000;
	static uint32_t prev_error	   	= 0;
	static uint8_t	isInReverse		= 0;
	if (timeDone)
	{
		OS_Kill();
	}

	if (!done)
	{
		done = 1;
		OS_ClearMsTime();
	}

	while(1)
	{
		IR_DATA* nextData;
		OS_Wait(IRCount);
		OS_Wait(IRMutex);
		uint8_t success = IRDataFifo_Get((uint32_t*) &nextData);
		OS_Signal(IRMutex);

		/* NOTE: Get the ratio between IR sensors */
	    latestIRLeft = nextData->IRSensorOne;
	    latestIRRight = nextData->IRSensorZero;


		if (nextData->IRSensorTwo < nextData->IRSensorThree)
	    {
			isInReverse = 0;
	    		if (nextData->IRSensorTwo < IR_THRESHOLD)
	    		{
	    			//OS_SetSteeringAngle(DEAD_CENTER + 600);
	    			if (prev_error != 0)
	    			{
	    				/*
	    				uint32_t currentImpulse 	= prev_error - (error);
	    				*/
	    				uint32_t error 			= IR_THRESHOLD - nextData->IRSensorTwo;
	    				prev_error = error;
	    				turnMultiplier -= 10;
	    				turnMultiplier = (turnMultiplier < 50) ? 700 : (turnMultiplier > 1000) ? 1000 : turnMultiplier;

	    			}
	    			else
	    			{
	    				prev_error = IR_THRESHOLD - nextData->IRSensorTwo;
	    				turnMultiplier = 1000;
	    			}

	    			OS_SetLeftMotorDir(STR_SPEED * turnMultiplier, FORWARD);
	    			OS_SetRightMotorDir(TRN_SPEED * turnMultiplier, BACKWARD);

	    		}
	    		else
	    		{
	    			OS_SetLeftMotorDir(STR_SPEED * speedMultiplier, FORWARD);
	    			OS_SetRightMotorDir(STR_SPEED * speedMultiplier, FORWARD);
	    			prev_error = 0;
	    		}
	    }
	    else
	    {
			if (nextData->IRSensorThree < IR_THRESHOLD)
			{
					//OS_SetSteeringAngle(DEAD_CENTER - 600);

    			if (prev_error != 0)
    			{
    				uint32_t error = IR_THRESHOLD - nextData->IRSensorTwo;
    				prev_error = error;
    				turnMultiplier -= 10;
    				turnMultiplier = (turnMultiplier < 50) ? 700 : (turnMultiplier > 1000) ? 1000 : turnMultiplier;

    			}
    			else
    			{
    				prev_error = IR_THRESHOLD - nextData->IRSensorTwo;
    				turnMultiplier = 1000;
    			}


				OS_SetLeftMotorDir(TRN_SPEED * turnMultiplier, BACKWARD);
				OS_SetRightMotorDir(STR_SPEED * turnMultiplier, FORWARD);
			}
			else
			{
				EnableTask1();
				OS_SetLeftMotorDir(STR_SPEED * speedMultiplier, FORWARD);
				OS_SetRightMotorDir(STR_SPEED * speedMultiplier, FORWARD);
    				prev_error = 0;

			}

	    }

		IRBaseLeftFifo_Put(nextData->IRSensorOne);
		IRBaseRightFifo_Put(nextData->IRSensorZero);
		OS_DestroyAllocation(nextData, sizeof(IR_DATA));

	}
}

#define PF3       (*((volatile uint32_t *)0x40025020))
void SonarController(void)
{
	while(1)
	{
		PING_DATA* nextData;
		OS_Wait(SonarCount);
		OS_Wait(SonarMutex);
		uint8_t success = SonarDataFifo_Get((uint32_t*) &nextData);
		OS_Signal(SonarMutex);
		PF3 ^= 0x08;

		latestSonar = nextData->PingSensorZero;
		OS_Signal(SonarDependency);
		if (nextData->PingSensorZero < 30)
		{
			speedMultiplier = 36 * nextData->PingSensorZero - 96;
		}
		else
		{
			speedMultiplier = 1000;
		}

		OS_DestroyAllocation(nextData, sizeof(PING_DATA));

	}
}



