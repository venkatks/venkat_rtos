/*
 * Jitter.h
 *
 *  Created on: Mar 3, 2016
 *      Author: Venkat
 */

#ifndef NRTOS5_OS_KERNEL_EVALUATIONMANAGER_JITTER_H_
#define NRTOS5_OS_KERNEL_EVALUATIONMANAGER_JITTER_H_

void DAS(void);
void Jitter();
void JittertPrint();


#endif /* NRTOS5_OS_KERNEL_EVALUATIONMANAGER_JITTER_H_ */
